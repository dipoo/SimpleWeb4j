package org.arong.web.action;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.arong.simpleweb4j.BaseAction;
import org.arong.simpleweb4j.PageUtil;

public class TestAction extends BaseAction {
	public String comeon(){
		request.setAttribute("message", "hello!你的SimpleWeb4j已经可以工作了！");
		return "comeon";
	}
	public String register(){
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		request.setAttribute("username", username);
		request.setAttribute("password", password);
		return "register";
	}
	public void ajax(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		PageUtil.printText(sdf.format(new Date()));
	}
}
