package org.arong.simpleweb4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IAction {
	String work(HttpServletRequest req, HttpServletResponse resp);
}
