package org.arong.simpleweb4j;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

public class PageUtil {
	private static HttpServletResponse response;
	
	public static void setResponse(HttpServletResponse response) {
		PageUtil.response = response;
	}
	
	/**
	 * 
	 */
	public static void printText(String text){
		response.setContentType("text/html;charset=utf-8");
		try {
			response.getWriter().write(text);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
