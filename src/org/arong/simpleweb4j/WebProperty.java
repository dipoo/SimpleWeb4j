package org.arong.simpleweb4j;

import javax.servlet.http.HttpServletRequest;

public class WebProperty {
	private static HttpServletRequest request;
	public static void setRequest(HttpServletRequest req) {
		request = req;
	}
	public static String getRealPath(){
		return request.getSession().getServletContext().getRealPath("/");
	}
}
