<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'index.jsp' starting page</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
    <link rel="stylesheet" href="pure-min.css" type="text/css"></link></head>
  
  <body>
	  <form class="pure-form" target="_blank" action="${pageContext.request.contextPath}/SimpleWeb4j" method="get" style="width:500px;margin:0 auto;">
	  	<fieldset>
	  		<legend>小试牛刀</legend>
	  		<a target="_blank" href="${pageContext.request.contextPath}/SimpleWeb4j?method=comeon">SimpleWeb4j用起来</a>
	  	</fieldset>
	  	<fieldset>
	  		<legend>表单应用</legend>
	  		<input type="hidden" name="method" value="register" />
	  		<input class="pure-text" type="text" name="username" placeholder="用户名" />
	  		<input class="pure-password" type="password" name="password" placeholder="密码" />
	  		<input type="submit" class="pure-button pure-button-primary" style="font-size: 85%;" value="注册" />
	  	</fieldset>
	  	<fieldset>
	  		<legend>ajax应用</legend>
	  		<input type="button" onclick="getCurrentTime();" class="pure-button" style="font-size: 85%;" value="获取服务器当前时间" />
	  		<span id="currentTime"></span>
	  		<script type="text/javascript">
	  			function getCurrentTime(){
					ajax({
						url : "${pageContext.request.contextPath}/SimpleWeb4j?method=ajax",
						success : function(response){
							document.getElementById("currentTime").innerHTML = response;
						}
					});
				}
	  			function createAjax() {
					var xhr = null;
					try {
						xhr = new ActiveXObject("microsoft.xmlhttp")
					} catch (e1) {
						try {
							xhr = new XMLHttpRequest()
						} catch (e2) {
							window.alert("does not support ajax")
						}
					}
					return xhr;
				}
				function ajax(conf) {
					var type = conf.type;
					var url = conf.url;
					var data = conf.data;
					var dataType = conf.dataType;
					var success = conf.success;
					var ele = conf.element;
					if (type == null) type = "get";
					if (dataType == null) dataType = "text";
					var xhr = createAjax();
					xhr.open(type, url, true);
					if (type == "GET" || type == "get") {
						xhr.send(null)
					} else if (type == "POST" || type == "post") {
						xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
						xhr.send(data)
					};
					xhr.onreadystatechange = function() {
						if (xhr.readyState == 4 && xhr.status == 200) {
							if (dataType == "text" || dataType == "TEXT") {
								if (ele != null) ele.html(xhr.responseText);
								if (success != null) success(xhr.responseText)
							} else if (dataType == "xml" || dataType == "XML") {
								if (success != null) success(xhr.responseXML)
							} else if (dataType == "json" || dataType == "JSON") {
								if (success != null) success(eval("(" + xhr.responseText + ")"))
							}
						}
					}
				}
	  		</script>
	  	</fieldset>
	  </form>
  </body>
</html>
